#!/bin/python
# -*- coding: cp1251 -*-

import argparse
import sys
import pprint
from struct import pack, unpack

#########################################################################################
def DepthJMVCToGML( byte, w, px ):
    Znear   = -23.394160
    Zfar    = -172.531931

    fu = 1732.875727
    fv = 1729.908923
    du = 943.231169
    dv = 548.845040
    
    t1_5 = 7.965116
    t1_6 = 9.558140
    t1_7 = 11.151163

    ##############
    v = byte
    zr = 1 / ( v / 255.0 * ( 1 / Znear - 1 / Zfar ) + 1 / Zfar );
    RealD = fu * ( t1_5 - t1_7 ) / zr
    vGML = 100 * 100 * 250 * RealD / ( w * px )
    return max( min( RealD + 128, 255 ), 0 )
#########################################################################################
def Main( argv ):
    parser = argparse.ArgumentParser(
        description='YUV trimmer',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=
        'Usage example:\n'
        '    python trim_yuv.py input_file.yuv output_file.yuv 1920 1088 10 25 0 yuv444\n'  
        )
    parser.add_argument('input video', action='store', type=str )
    parser.add_argument('output video', action='store', type=str )
    parser.add_argument('width', action='store', type=int )
    parser.add_argument('height', action='store', type=int )
    parser.add_argument('start', action='store', type=int )
    parser.add_argument('end', action='store', type=int)
    parser.add_argument('append', action='store', type=int, choices=[0, 1], default=0, nargs='?')
    parser.add_argument('format', action='store', type=str, default='yuv420', nargs='?', choices=('yuv444', 'yuv420', 'y') ) 
    parser.add_argument('--verbose', action='store_const', const=True, default=False )
     
    params = vars( parser.parse_args( argv[1:] ) )
   
    pathFrmSrc = str( params['input video'] )
    pathFrmDst = str( params['output video'] )
    w = int( params['width'] )
    h = int( params['height'] )
    start = int( params['start'] )
    end   = int( params['end'] )
    append = int( params['append'] )
    formatImg   = str( params['format'] )

    px = 1000
    
    if( w <= 0 or h <= 0 ):
        raise Exception( "Width and height must be positive" )
    
    arraySizesY  = { 
        'yuv420': w * h, 
        'yuv444': w * h , 
        'y' : w * h }
    arraySizesUV = { 
        'yuv420': w * h / 4, 
        'yuv444': w * h, 
        'y' : 0 }

    sizeY  = arraySizesY [ formatImg ]
    sizeUV = arraySizesUV[ formatImg ] * 2
    sizeYUV = sizeY + sizeUV

    fin = open( pathFrmSrc, "rb" )
    fout = open( pathFrmDst, "ab" if append else "wb" )
    count = 0
    try:
        fin.seek( start * sizeYUV, 0 )
        if( fin.read( 1 ) == "" ):
            raise Exception("Unable to find start frame {frame} in file {pathFrmSrc}".format( frame=start, pathFrmSrc=pathFrmSrc ) )
        
        fin.seek( end * sizeYUV, 0 )
        if( fin.read( 1 ) == "" ):
            raise Exception("Unable to find end frame {frame} in file {pathFrmSrc}".format( frame=end, pathFrmSrc=pathFrmSrc ) )
    
        for i in range( start, end + 1 ):
            if( params['verbose'] ):
                print ( "Copying frame {frame}".format( frame=i ) )
            
            posIn = i * sizeYUV
            fin.seek( posIn, 0 )
            
            if( fin.tell() != posIn ):
                raise Exception("Unable to read frame {frame} from file {pathFrmSrc}".format( frame=i, pathFrmSrc=pathFrmSrc ) )
            
            bytesY_read  = fin.read( sizeY )
            bytesUV_read = fin.read( sizeUV )
            if( len( bytesY_read ) != sizeY or len( bytesUV_read ) != sizeUV ):
                raise Exception("Unable to read frame {frame} from file {pathFrmSrc}".format( frame=i, pathFrmSrc=pathFrmSrc ) )
            
            bytearr = unpack('<%dB'%len(bytesY_read), bytesY_read)
            print type( bytearr )

            pixels = [DepthJMVCToGML( byte, w, px ) for byte in bytearr]
            #for b in bytearr:
            #    print b, ' ', DepthJMVCToGML( b, w, px )
    
            for i in range( len( bytearr ) ):
                #sys.stdout.write( str( bytearr[i] ) + " " )
                #bytearr[i] = bytearr[i] * 2
                pass
            
            bytesY_write = pack('<%dB'%len(pixels), *pixels)
            fout.write( bytesY_write )
            fout.write( bytesUV_read )
            count += 1
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
        raise
    except:
        print "Unexpected error:", sys.exc_info()[0]
        raise
    
    if( params['verbose'] ):
        print ( "{0} frames copied".format(count) )
            
        
        
#########################################################################################
if __name__ == '__main__':
    Main( sys.argv )