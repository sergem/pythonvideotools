#!/bin/python
# -*- coding: cp1251 -*-

import argparse
import sys
import os
import subprocess


#########################################################################################
def Main( argv ):
    parser = argparse.ArgumentParser(
        description='AvsAsAvi. mount avs as avi',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=
        'Usage example:\n'
        '    python avsasavi.py (mount|unmount) input_file.avs\n'  
        )
    parser.add_argument('action', action='store', type=str, choices=('mount', 'unmount') ) 
    parser.add_argument('input video', action='store', type=str )
    parser.add_argument('--verbose', action='store_const', const=True, default=False )
     
    params = vars( parser.parse_args( argv[1:] ) )
   
    pathFrmSrc = str( params['input video'] )
    action     = str( params['action'] )

    try:
        ( pathDir, filename ) = os.path.split( pathFrmSrc )

        argNew = [ "pfm", action, filename ]
        p = subprocess.Popen( argNew, shell = True, cwd = pathDir )
        res = p.wait()

    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
        raise
    except:
        print "Unexpected error:", sys.exc_info()[0]
        raise
    
    if( params['verbose'] ):
        print ( "Finished" )
            
        
        
#########################################################################################
if __name__ == '__main__':
    Main( sys.argv )