#!/bin/python
# -*- coding: cp1251 -*-

import argparse
import sys
import pprint


#########################################################################################
def Main( argv ):
    parser = argparse.ArgumentParser(
        description='YUV trimmer',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=
        'Usage example:\n'
        '    python trim_yuv.py input_file.yuv output_file.yuv 1920 1088 10 25 0 yuv444\n'  
        )
    parser.add_argument('input video', action='store', type=str )
    parser.add_argument('output video', action='store', type=str )
    parser.add_argument('width', action='store', type=int )
    parser.add_argument('height', action='store', type=int )
    parser.add_argument('start', action='store', type=int )
    parser.add_argument('end', action='store', type=int)
    parser.add_argument('append', action='store', type=int, choices=[0, 1], default=0, nargs='?')
    parser.add_argument('format', action='store', type=str, default='yuv420', nargs='?', choices=('yuv444', 'yuv420', 'y') ) 
    parser.add_argument('--verbose', action='store_const', const=True, default=False )
     
    params = vars( parser.parse_args( argv[1:] ) )
   
    pathFrmSrc = str( params['input video'] )
    pathFrmDst = str( params['output video'] )
    w = int( params['width'] )
    h = int( params['height'] )
    start = int( params['start'] )
    end   = int( params['end'] )
    append = int( params['append'] )
    formatImg   = str( params['format'] )
    
    if( w <= 0 or h <= 0 ):
        raise Exception( "Width and height must be positive" )
    
    sizes = { 'yuv420': w * h * 3 / 2, 'yuv444': w * h * 3, 'y' : w * h }
    fsize = sizes[ formatImg ]
    
    fin = open( pathFrmSrc, "rb" )
    fout = open( pathFrmDst, "ab" if append else "wb" )
    count = 0
    try:
        fin.seek( start * fsize, 0 )
        if( fin.read( 1 ) == "" ):
            raise Exception("Unable to find start frame {frame} in file {pathFrmSrc}".format( frame=start, pathFrmSrc=pathFrmSrc ) )
        
        fin.seek( end * fsize, 0 )
        if( fin.read( 1 ) == "" ):
            raise Exception("Unable to find end frame {frame} in file {pathFrmSrc}".format( frame=end, pathFrmSrc=pathFrmSrc ) )
    
        for i in range( start, end + 1 ):
            if( params['verbose'] ):
                print ( "Copying frame {frame}".format( frame=i ) )
            
            posIn = i * fsize
            fin.seek( posIn, 0 )
            
            if( fin.tell() != posIn ):
                raise Exception("Unable to read frame {frame} from file {pathFrmSrc}".format( frame=i, pathFrmSrc=pathFrmSrc ) )
            
            bytes_read = fin.read( fsize )
            if( bytes_read == "" ):
                raise Exception("Unable to read frame {frame} from file {pathFrmSrc}".format( frame=i, pathFrmSrc=pathFrmSrc ) )
            
            fout.write(bytes_read)
            count += 1
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
        raise
    except:
        print "Unexpected error:", sys.exc_info()[0]
        raise
    
    if( params['verbose'] ):
        print ( "{0} frames copied".format(count) )
            
        
        
#########################################################################################
if __name__ == '__main__':
    Main( sys.argv )